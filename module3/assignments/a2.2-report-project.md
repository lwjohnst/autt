# Assignment 2 instructions

In this module your are asked to describe and document the results and
experiences from your project in a report that you share and discuss with your
fellow participants in your project group. It is our intention that your report
may serve as a draft version for a component in your teaching portfolio.

You are welcome to use the template used on course day 1 and for your
supervision also as the template for your report.

Note on using your report in your teaching portfolio:

In some contexts you may be asked to describe the "reasons for your own choices
in relation to teaching" and describe and document the results of completed
teaching as part of your teaching portfolio.

Therefore we suggest that you consult the guidelines for teaching portfolios at
Aarhus University (and possible the local guidelines at your department) and
keep those in mind when you write your report. You find the portfolio guidelines
here: Portfolio guidelines (on Blackboard).

- Prepare and upload your report in a blog post in your group blog. You find the
group working area by clicking on the title "Project work: Report"
- Make sure to include your name in the file name
