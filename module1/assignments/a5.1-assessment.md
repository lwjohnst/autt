# Assignment 5.1

> Assessment is probably the most important thing we can do to help our students
to learn (Brown, 2005)

There are many good reasons for why we assess our students' learning. Quality
assessment activities, however, are not easy to develop. As teachers we decide
how we assess, when we assess and what we assess, and at the same time, we
should also have an idea of why we assess. Traditionally, there has been a
tendency to assess in order to distinguish between students, to make sure
students engage in teaching activities, and to provide documentation of
students' performance. Recently, however, a new approach to assessment has
emerged in higher education in Denmark; namely, the idea that we assess student
performance in order to support and strengthen student learning. This idea has
been termed 'assessment for learning' and is tightly linked to (actionable)
feedback, transparent assessment criteria and a bit of thinking outside the box.

During the residential course at Sandbjerg, you will be asked to develop an
assessment activity that you can use in your own course. The assessment activity
may be graded or non-graded. You will be asked to consider if and how the
students will receive feedback, and if and how you will use assessment criteria.
You will receive peer feedback on you assessment activity.

# Instructions:

- Read Brown (2005). Assessment for learning. Learning and Teaching in Higher
Education, Issue 1, pp. 81-89

- Brainstorm and sketch your initial ideas for an assessment activity in your
course including feedback to students. You might find ideas to the activity in
section 4.4 in Your Course Description.

- Bring your notes on day 2 of the residential course.

## Brown 2005 notes

- Assessments should be learner-centred and should reflect the learner-centred 
curriculum.
    - Should focus on evidence of achievement (so less focus on exams and other
    traditional assessments)
    - Assessment should be valid, meaning that it reflects what is intended to be
    learnt
    - Should be practice-orientated to demonstrate employability, so should show
    putting the learning to practice/use
    - Should be authentic, meaning they assess what they claim to assess
    - Should be efficient for staff time, cost-effective for organizations paying,
    and should be managable and achievable
    - Should be given regularly to allow for improving based on feedback
    - Should be inclusive by using mutliple types of assessments
    - Clear, explicit assessment criteria
    - Have inter-grader reliability
    - Be developmental, not judgemental (assessments should encourage growth and
    development)
- Assessments should emphasize around giving feedback
    - Need feedback so they know what they did well, what they can improve, what
    to focus on, etc
    - Formative vs summative feedback...?

## Brainstorm assessment ideas

- Right now the assessment activity is a short analysis project report and a
short presentation on it. The assessment is not "formal" in that it is not
"graded".
    - This assessment relates directly to what the course is about.
    - Completing the assessment gives the learners practice on what they learn,
    in a real-world way.
    - I normally give some short feedback on it, but could give more and give 
    feedback throughout the days during the exercise sessions.
        - For instance, during the exercises, I could go to each group and check
        in on them and see how they are doing with the material and provide
        immediate feedback then.
