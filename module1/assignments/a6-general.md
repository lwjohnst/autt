# Introduction to teaching methods

This section gives you a brief introduction to three methodological archetypes,
which will be discussed during the third day of the residential course:

- lecture
- small class teaching
- supervision

The strengths and weaknesses of each archetype show why, in most cases, a
course/module must use all three archetypes to support students' learning at an
adequate taxonomic level.

## The three didactic archetypes

Although teaching often accentuates one dimension at the expense of the others,
all three dimensions are always present. Even when the teacher's interaction
with content is the most prominent - for example, during monologues - the
students interact with both the teacher (they listen or interrupt) and the
content (they pay more or less attention).

Nevertheless, each archetype has its specific didactic  strengths and weaknesses
and in most cases a course/module must use a variety of methods to help students
to achieve the objectives of the course. In Meyer (2005, P. 17-18) 'variety of
teaching methods' comes out as 6th on a list of the 10 most influential factors
on students' achievement.

- Teaching as representation of content
    - Teaching focuses on the teacher as mediator of content and the manner in
    which content is presented to the students. The aim is to demonstrate or
    show something to the student.
    - Examples include: Lecture, Demonstration, Video
    - Strengths: Gives an overview, opens a new field of knowledge (lower
    taxonomic levels)
    - Weaknesses: No focus on the student’s understanding 

- Teaching as interplay
    - Teaching focuses on the interplay and dialogue between the teacher and the
    student or between students (peers). This archetype covers a range of
    different roles and relations between the teacher and the student. At one
    end, we find the teacher as representative of a fixed body of knowledge
    (Teacher as master). At the opposite end, the teacher and the students have
    more equal positions in defining the content and its meaning (Teacher as
    'gardener' or 'midwife').
    - Examples include: Dialogue, Questioning, Supervision, Coaching
    - Strengths: Opportunity to explore content and understandings (higher
    taxonomic levels)
    - Weaknesses: Clarity in aim and content might decrease as a consequence of
    the dynamics of the conversation

- Teaching as experience
    - Teaching focuses on and urges the students to interact with content.
    Accordingly, this dimension has a clear focus on different types of
    activities, ranging from highly content-oriented activities, such as
    pre-defined assignments, to more open tasks and exercises, and to student
    driven projects.
    - Examples include: Training, Multiple choice exercises, Assignments, Case
    based teaching, Problem based, and/or problem solving teaching
    - Strengths: Training of knowledge, skills and competences; Problem solving
    (higher taxonomic levels)
    - Weaknesses: If no feedback, risk of learning the wrong things

### Lectures

In a traditional lecture information is transmitted from the teacher to the
students who listen or take notes. This particular teaching method has its own
advantages including the possibility of creating an overview of a subject area
and reaching many students at the same time. However, the learning outcome from
the lecture alone is rarely impressive if students merely listen and take notes.
For this reason, many teachers perform an active lecturing style in which
students are involved in more than just listening. On day three of the
residential course, one of us will give an active lecture on lecturing.

### Small class teaching

Small class teaching has many different names (seminars, tutorials, classroom
teaching, small group teaching) reflecting the different ways of organizing
teaching. In Danish this type of teaching also has a variety of names such as
"holdundervisning", "teoretiske øvelser", "problemregning", "seminarer", etc. In
the teacher training programme, small class teaching refers to teaching
situations in which students (approx. 20-40 students) work actively on the
course content, train practical skills or practice oral communication or
critical thinking. A few examples of teaching and working methods are given
below.

- Student presentations: Students present scientific papers, course content or
parts of the course content. Students not presenting can be appointed as
opponents, feedback givers or audience. The presentation can be followed by a
discussion.

- Theoretical exercises and problem solving: Students apply methods and concepts
in problem solving. Problems are provided by the educator and can be solved in
advance or in class. Solutions and/or problem solving processes might be
discussed in class and often presented at the blackboard.

- Discussion-based teaching: The educator involves students in a dialogue about
the course content. The dialogue can be based on questions answered by students
in advance.

- Case-based teaching: Students work on 'real-life' problems and make decisions
based on the available information (provided by educator, companies or own
research) and theories within the discipline. Cases to be solved are provided by
the educator or a third party, e.g. a company or an organisation.

- Problem-based learning: Problems to be solved are provided by the educator or
a third party, e.g. a company or an organisation.

- Laboratory teaching: Students perform practical procedures or experiments in
the laboratory. The laboratory instruction is often developed by the educator
but can also be influenced by students (freedom of choice of problem, methods or
results).

- Computer laboratory teaching: Students work in a computer laboratory on
problem solving, programming, or software application.

- Peer assessment: Students give feedback on drafts/peer assignments/tests
with/without a model answer or rubric from the educator.

- Cooperative learning: Students cooperate in small groups through structured
activities.

- Excursions/field trip: Students are taken into the ‘real world’ to get
experience on site or to perform field experiments.

- Internship: Students work outside the university for a period of time i.e.
1-3-6 months as part of their degree programme. Their work during the internship
is supervised by an educator.

### Supervision

When it comes to supervision, many students are occupied with getting answers
and most supervisors are quite good at providing these answers. This is normal
and perfectly acceptable.

However, if we as supervisors want to improve the quality of our advice and if
we want to foster students’ sense of independence and ownership, we need to
learn to ask good questions, too.

This session offers a brief introduction into a conversation model developed by
Gitte Wichmann-Hansen and Tine Wirenfeldt Jensen. You will be practicing the
model on your colleagues, thus, please read the short excerpt carefully.
