
# Assignment 4.1-4.3 instructions:

- Now you should have completed the template. 

- Save the template as a pdf. A pdf makes it much easier to open and download the
assignments. Remember to name it so that it easily identified, e.g. name_course
description.

- Upload the template at file exchange in your group

- Read the course description of your group members and write comments and
questions that emerge during reading before arriving at Sandbjerg.
