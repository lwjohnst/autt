
# Assignment 4.1-4.3 preparation:

Read others courses and make some notes.

## Lorenzo

- Brief description: about livestock nutrition
- Questions:
    - Are there are teaching styles used besides lectures?

## Katarina

- Brief description: About the treaty on internal trade in EU
- Comments: 
    - LO could be more clear (e.g. what does "account" mean?)
    - Formal assessment could be usefully done throughout the course, rather
    than at the end

## Kristine

- Brief description: Theories of communication and language
- Comments:
    - LO could be more explicit and descriptive (e.g. what does "document insight"
    mean?)
- Questions:
    - Think only one formal assessment (final exam) is appropriate? Could it be 
    more improved if there were multiple, smaller formal assessments with feedback
    at each one?

## Jo

- Brief description: Microbiology and industry
- Comments:
    - LO could be more explicit and descriptive (e.g. what does "possess a
    vision" mean?)
- Question:
    - What is this internal/external sensor? (From the Danish government.)

## Natalie

- Brief description: Global burden of disease overview
- Comments:
    - LO could be more descriptive/explicit, e.g. define "understand"
    
