
i) research area and interests
ii) teaching experience
iii) expectations for the course

Hi everyone,

My name is Luke Johnston. I am a postdoc working in the Department of Public
Health in diabetes epidemiology. My research/work interests lie in mainly three
areas: how exposure to adverse conditions during early life can impact health in
later adult, primarily with regard to diabetes; how to improve usage and aware
of open scientific and reproducible practices among biomedical scientists; and,
how to best teach scientists the best practices of data analysis from an open
scientific and reproducible framework.

Given my interests in how to best teach computational/analytic thinking and
programming, I have a fair amount of experience teaching researchers these
skills. I'm a Software and Data Carpentry Instructor, I've taught many coding
workshops in Denmark, Germany, and Canada, and wherever I work I try to set up
short and frequent training sessions on coding (e.g. here at Aarhus University
and at the University of Toronto). Aside from coding courses and workshops, I've
also taught Nutritional Sciences courses.

My expectation for taking this course is to keep updated on the science of
pedagogy and best practices for teaching. I've previously taken an Advanced
Teaching course during my PhD, which I found extremely useful, so I'm looking
forward to learning and updating my knowledge.

See you all later in August!
