# Objective

<!-- Bigger picture overview of the course. -->

## Original:

Students will develop proficiency in using the R statistical computing language,
as well as improving their data and code literacy.

## Updated:

This course will cover 


3. Know how and where to find help and to continue learning methods and 


# Learning outcomes

## Original:

1. An understanding of why an open and reproducible data workflow is important.
2. Practical experience in setting up and carrying out an open and reproducible
data analysis workflow.
3. Know how to continue learning methods and applications in this field.

## Updated:

1. To explain the importance of a reproducible data analysis in achieving
rigorous and ... scientific research.
2. To apply the basics (foundations? fundamentals?) of conducting an open,
collaborative, and reproducible data analysis project.

# Notes

Part 2 could be more detailed on the reproducibility.

Assessing knowledge on reproducibility and openness with
survey/questionnaire/mcq
