---
output: html_document
---

# Assessment Activity

- Presently the assessment activity is a short report on an simple analysis
project report with a brief oral presentation on it
- Report needs to be done in order to present and presenting is done at the end
of the three day course
- The report is assessed for being reproducible, for following and including 
tools used from the content (and even new tools used is a bonus), and for
having a standard project folder structure.
- This assesses whether learners have applied the concepts and skills taught in
class to a real-world dataset. By completing the assessment activity, the
students will have practice in using the skills and knowledge so that they may
use them in their own work.

# Assessment Feedback

- I previously gave some brief verbal feedback immediately after the group
presented, mostly on what was nice what they did and one suggestion for the 
future.
    - I also ask questions to see what they did, what they struggled with, what
    they could improve on, why they chose the analyses or methods that they did.

- In the future, I will provide a simple "rubric" on what a good project,
report, and presentation should be like and what I expect, as an example that
they may follow.
- I will (maybe) continue giving feedback right after the presentation, but I
may consider adding peer written feedback that each group gets at the end of the
session.
    - Otherwise, I could write down the feedback.
    - Feedback will include what worked, what could be improved, and (maybe) what
    was memorable.
    - Other instructors can also provide feedback.
