# Notes from Sandbjerg

- Rubric for both assessment of and from students
    - Could be included to get feedback from students on how to improve the course
- Student peer assessments
    - Activities that have students making them..?
- Continuous assessment
    - Measure progress multiple times throughout the course (for both teacher
    knowledge and student feedback)
    - encourage continuous learning over the typical "study and dump" for exams
    - provides feedback earlier

- Feedback
    - Info on where student is at, where students are going, how do they get there
    - Know how to give and receive feedback
    - Should be constructive-specific-considerate
    
- Lecturing
    - Structure: 
        - Important things in beginning
        - Keep it organized
        - Prepare and plan beforehand
        - Focus their attention on images/etc
        - Break down into chunks
    - Context:
        - Give meaning
        - Focus attention
        - Connect to prior knowledge
    - Active learning:
        - Include active participation
        - Small thought exercises
        - Paired work
        - Interaction with students
    - Repetition:
        - Repeat important or key items throughout lecture, especially what you 
        want them to remember
