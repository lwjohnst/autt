---
output: html_document
---

## Things to improve

- More time spent on and more guidance about using version control (Git). The
content should be more complete then it is right now and there currently are
few exercises on this section.
    - TODO: I need to update and add more content explaining this, especially adding
    relevant exercises.
    - TODO: Be more structured on this section, as it can get confusing fast.
- Incorporate the exercises more into the final project, so that as they practice
what was just recently taught, they can also work towards finishing their project.
    - TODO: Revise exercises to incorporate final project so that the project
    can be worked on as part of the exercise.

## Things to continue

- My enthusiasm and passion about the topic is always an area of good feedback,
so I think I'll keep being myself ;)
- Providing the course content online in a super easy and accessible way is something
that I feel is very important, it also keeps the curriculum development pipeline smooth,
and the learners also really like it.
