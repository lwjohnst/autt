# Resources and useful links

- As promised follows links to some of the resources:
    - AU Educate - the most elaborate. An Arts initiative but and from 2019 a joint venture between all four centre:  http://educate.au.dk/
    - STLL ressources: http://stll.au.dk/ressourcer/
    - BSS - educatinal technologiy: https://treat.au.dk/
- A resource for students:  http://studypedia.au.dk/
- Students with special needs :  https://www.au.dk/raadgivnings-og-stoettecentret/

