# Assignment 3c instructions

Sum up the key messages from the feedback you received in a post in your learning portfolio
	
## Key messages

- May be hard to assess impact of change, since course is getting a structural
overhaul as well. (previously 4 days split into two with two weeks break, now 3 
consecutive days.)
- Make sure to get instructor/helper feedback.
- Give plenty of time to complete the pre-workshop task, have an early deadline,
and provide a way to contact you/confirm they did it. Video calling with
screensharing would be useful to help those who have trouble.
- Provide a description for why set-up should be done before the course, so students
can understand that it can help speed up getting into the more relevant and interesting
material.
- Include both video screencast and written text to go over the pre-workshop tasks,
for those who would rather read than watch.
