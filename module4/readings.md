
# Portfolios used in appnt for academic positions

https://medarbejdere.au.dk/en/administration/hr/recruitmentandonboarding/employment/academic-staff/teaching-portfolios/

Notes:

- Portfolio is concrete documentation and description of teaching experience and
evaluations.
- At AU, portfolio contains at least: Description and documentation (required),
Reasons for choices in relation to teaching (optional), and results (optional)
- There are several required elements to the description and doc (see portfolio.Rmd)

# Pedagogical competence profile

https://uddannelseskvalitet.ku.dk/quality-assurance-of-study-programmes/development-initiatives/teaching-competencies/pedagogical-competence-profile/

Notes: 

- There are six or so "competences" (can't click on last competency).

1. Areas of responsibility:
    - Responsibilities such as running courses organized by others to developing
    your own to cooperating with others to make new courses/material to higher 
    level thinking and planning around programme level development.

2. Knowledge sharing and peer supervision:
    - Takes place in different ways with different aims.
    - One aim is to develop as teacher
    - Another is to develop the teaching quality of others.
    - And another might be to contribute to knowledge sharing on broader organizational,
    societal, and international levels.
    - Can achieve these by:
        - Improving teaching based on feedback
        - Being active in team teaching
        - Communicating and sharing with others about programme level teaching
        - Being a teaching mentor to other teachers/colleagues
        - Participating in discussions on how society influences education and on
        challenges and development of educational material
        . Contributing to creating teaching material, pedagogy, and didactics at
        all levels.

3. Knowledge of learning, teaching, and the study programme:
    - Seen in context of "practice and reflection", since knowledge is only 
    relevant when put into practice.
    - So knowledge could be of how learning works, how students behave in the
    learning/teaching environment, the design of the material/curriculum, how
    the education/training influences labour market and vice versa (understanding
    societal context of teaching, needs and demands), interaction of programme
    with bigger picture (other programmes, university goals, etc), linking
    research with teaching, evaluation of teaching, etc.

4. Practice and reflection:
    - Ability to intentionally and continually establish and develop good
    teaching practices.
    - Linked to "knowledge" (3)
    - Good teaching practice includes: focus on learning outcomes, seek to understand
    students' needs and desires, clear learning goals and course alignment,
    formative assessment (give feedback to students on learning), design assessments
    to reflect material and learning outcome/objectives, link with programmes overall
    goals and structure, link research on learning to teaching, constantly updating
    and refining teaching material and teaching in general.

5. Training in the pedagogy of university teaching
    - Formal qualifications and personal teaching development.
    - Such as by taking courses, workshops, peer discussion, read on latest
    teaching research.
