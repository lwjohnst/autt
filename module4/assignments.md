# A1: Draft of your teaching portfolio

This activity prepares you for working with your teaching portfolio on module 4.
It takes some time to complete a teaching portfolio, so we will recommend you to
set aside at least 5-6 hours for this activity.

The goal is a draft of your teaching portfolio with some text in all 7 points in
the often-used standard (see the attached file).

Please submit the draft of your teaching portfolio before 9th of December at
12.00. Click the heading to submit.

Bring your teaching portfolio and a computer on the day of the course where you
will receive peer-feedback on your teaching portfolio and you will have time to
develop your teaching portfolio further.

# A2: Learning outcomes of AUTT

The purpose of this activity is to collect and compare points and impressions
from previous modules and from your own projects.

Please read your learning portfolio from the teacher training programme and
note 2-3 main learning outcomes in this blog.  Click the heading to to go to the
blog.
